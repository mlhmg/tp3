import java.io.*;
import java.util.*;

public class DonatRaya{
    private static InputReader in;
    private static PrintWriter out;
    private static int wilayahMenangSatu = 0;
    private static int wilayahMenangDua = 0;
    private static Integer[] persentasiMenangSatu = new Integer[101];
    private static Integer[] persentasiMenangDua = new Integer[101];
    private static AVLTree selisihTree = new AVLTree();

    public static void main(String[] args){
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

//        selisihTree.root = selisihTree.insert(selisihTree.root, 10);
//        selisihTree.root = selisihTree.insert(selisihTree.root, 20);
//        selisihTree.root = selisihTree.insert(selisihTree.root, 30);
//        selisihTree.root = selisihTree.insert(selisihTree.root, 40);
//        selisihTree.root = selisihTree.insert(selisihTree.root, 50);
//        selisihTree.root = selisihTree.insert(selisihTree.root, 25);
//
//
//        selisihTree.preOrder(selisihTree.root);


        for (int i = 0; i < 101; i++) {
            persentasiMenangSatu[i] = 0;
            persentasiMenangDua[i] = 0;
        }


        HashMap<String ,Node> listWilayah = new HashMap<>();
        ArrayList<String> listProvinsi = new ArrayList<>();




        String donatSatu = in.next();
        String donatDua = in.next();

        int jumlahWilayah = in.nextInt();
//        for (int i = 0; i < jumlahWilayah; i++) {
//            String namaWilayah = in.next();
//            if (listWilayah.get(namaWilayah) == null){
//                listWilayah.put(namaWilayah, new Node(null));
//            }
//            int jumlahSubWilayah = in.nextInt();
//            for (int j = 0; j < jumlahSubWilayah; j++) {
//                String namaSubWilayah = in.next();
//                listWilayah.put(namaSubWilayah, new Node(listWilayah.get(namaWilayah)));
//            }
//        }
        for (int i = 0; i < jumlahWilayah; i++) {
            String namaWilayah = in.next();
            if (listWilayah.get(namaWilayah) == null){
                listWilayah.put(namaWilayah, new Node(null));
                persentasiMenangSatu[50]++;
                persentasiMenangDua[50]++;
                selisihTree.root = selisihTree.insert(selisihTree.root, 0);
//                selisihTree.get(0).banyakWilayah++;

                int jumlahProvinsi = in.nextInt();
                for (int j = 0; j < jumlahProvinsi; j++) {
                    String namaProvinsi = in.next();
                    listWilayah.put(namaProvinsi, new Node(listWilayah.get(namaWilayah)));
                    listProvinsi.add(namaProvinsi);
                    persentasiMenangSatu[50]++;
                    persentasiMenangDua[50]++;
//                    selisihTree.get(0).banyakWilayah++;

                }

            } else {
                int jumlahSubWilayah = in.nextInt();
                for (int j = 0; j < jumlahSubWilayah; j++) {
                    String namaSubWilayah = in.next();
                    listWilayah.put(namaSubWilayah, new Node(listWilayah.get(namaWilayah)));
                    persentasiMenangSatu[50]++;
                    persentasiMenangDua[50]++;
//                    selisihTree.get(0).banyakWilayah++;

                }
            }
        }
        int banyakPerintah = in.nextInt();
        for (int i = 0; i < banyakPerintah; i++) {
            String perintah = in.next();
//            System.out.println(perintah);
//            switch (perintah){
//                case "TAMBAH":
//                    String namaKelurahanTambah = in.next();
//                    int suaraSatuTambah = in.nextInt();
//                    int suaraDuaTambah = in.nextInt();
//                    listWilayah.get(namaKelurahanTambah).tambahSuara(suaraSatuTambah, suaraDuaTambah);
//                    break;
//                case "ANULIR":
//                    String namaKelurahanAnulir = in.next();
//                    int suaraSatuKurang = in.nextInt();
//                    int suaraDuaKurang = in.nextInt();
//                    listWilayah.get(namaKelurahanAnulir).kurangSuara(suaraSatuKurang, suaraDuaKurang);
//                    break;
//                case "CEK_SUARA":
//                    String wilayahDiCek = in.next();
//                    System.out.println(listWilayah.get(wilayahDiCek).getSuaraSatu()+" "+listWilayah.get(wilayahDiCek).getSuaraDua());
//                    break;
//                case "WILAYAH_MENANG":
//                    String suaraMenang = in.next();
//
//                    break;
//                case "WILAYAH_MINIMAL":
//                    String suaraYangMenang = in.next();
//                    int minimalPersen = in.nextInt();
//                    break;
//                case "WLIAYAH_SELISIH":
//                    int selisihMinimal = in.nextInt();
//                    break;
//                case "CEK_SUARA_PROVINSI":
//                    break;
//            }

                if (perintah.equals("TAMBAH")){
                    String namaKelurahanTambah = in.next();
                    long suaraSatuTambah = in.nextLong();
                    long suaraDuaTambah = in.nextLong();
                    listWilayah.get(namaKelurahanTambah).tambahSuara(suaraSatuTambah, suaraDuaTambah);
                }
                else if (perintah.equals("ANULIR")){
                    String namaKelurahanAnulir = in.next();
                    long suaraSatuKurang = in.nextLong();
                    long suaraDuaKurang = in.nextLong();
                    listWilayah.get(namaKelurahanAnulir).kurangSuara(suaraSatuKurang, suaraDuaKurang);
                }
                else if (perintah.equals("CEK_SUARA")){
                    String wilayahDiCek = in.next();
                    out.println(listWilayah.get(wilayahDiCek).getSuaraSatu()+" "+listWilayah.get(wilayahDiCek).getSuaraDua());
                }
                else if (perintah.equals("WILAYAH_MENANG")){
                    String suaraMenang = in.next();
                    if (suaraMenang.equals(donatSatu)){
                        out.println(wilayahMenangSatu);
                    } else {
                        out.println(wilayahMenangDua);
                    }
                }
                else if (perintah.equals("WILAYAH_MINIMAL")){
                    String suaraYangMenang = in.next();
                    int minimalPersen = in.nextInt();
                    int banyakWilayah = 0;
                    if (suaraYangMenang.equals(donatSatu)){
                        for (int j = minimalPersen; j < 101; j++) {
                            banyakWilayah += persentasiMenangSatu[j];
                        }
                    } else if (suaraYangMenang.equals(donatDua)){
                        for (int j = minimalPersen; j < 101; j++) {
                            banyakWilayah += persentasiMenangDua[j];
                        }
                    }
//                    for (int j = 0; j < persentasiMenangSatu.length; j++) {
//                        System.out.print(j+""+persentasiMenangSatu[j]+" ");
//                    }
//                    for (int j = 0; j < persentasiMenangDua.length; j++) {
//                        System.out.print(j+""+persentasiMenangDua[j]+" ");
//                    }
//                    System.out.println(persentasiMenangSatu);
//                    System.out.println(persentasiMenangDua);

                    out.println(banyakWilayah);
                }
                else if (perintah.equals("WILAYAH_SELISIH")){
                    long selisihMinimal = in.nextLong();
                    int counter = 0;
                    for (Node wilayahNode : listWilayah.values()){
                        if (wilayahNode.getSelisih() >= selisihMinimal){
                            counter++;
                        }
                    }
                    out.println(counter);
                }
                else if (perintah.equals("CEK_SUARA_PROVINSI")){
//                    for (Map.Entry<String, Node> entry : listProvinsi.entrySet()){
//                        out.println(entry.getKey() + " " + entry.getValue().getSuaraSatu()+ " " +entry.getValue().getSuaraDua());
//                    }
                    for (String namaProvinsi : listProvinsi){
                        Node provinsi = listWilayah.get(namaProvinsi);
                        out.println(namaProvinsi+" "+provinsi.getSuaraSatu()+" "+provinsi.getSuaraDua());
                    }
                }
//            System.out.println(i);
        }
//        System.out.println(listWilayah);
        out.close();
    }

    static class Node{
        private Node parent;
        private long suaraSatu;
        private long suaraDua;

        public long getSelisih(){
            return Math.abs(suaraSatu-suaraDua);
        }
        public Node getParent() {
            return parent;
        }

        public long getSuaraSatu() {
            return suaraSatu;
        }

        public long getSuaraDua() {
            return suaraDua;
        }

        Node(Node parent){
            this.suaraSatu = 0;
            this.suaraDua = 0;
            this.parent = parent;
        }
        public void tambahSuara(long suara1, long suara2){
            if (suaraSatu > suaraDua){
                wilayahMenangSatu--;
            } else if (suaraSatu < suaraDua){
                wilayahMenangDua--;
            }
//            long selisihSebelum = this.getSelisih();
//            if(selisihTree.get(selisihSebelum) != null){
//
//            }

//            System.out.println(suaraSatu+" "+suaraDua);
            long temp1 = suaraSatu+suaraDua;
//            System.out.println((int)Math.floor((float)suaraSatu / temp1 * 100) + " "+ suaraSatu + " "+ suaraDua);
            if (suaraSatu+suaraDua != 0){
                persentasiMenangSatu[(int)Math.floor((double) suaraSatu / (double)temp1 * 100)]--;
                persentasiMenangDua[(int)Math.floor((double) suaraDua / (double)temp1 * 100)]--;
            }
            if (suaraSatu==0 && suaraDua == 0){
                persentasiMenangSatu[50]--;
                persentasiMenangDua[50]--;
            }

            suaraSatu += suara1;
            suaraDua += suara2;
            if (parent != null){
                parent.tambahSuara(suara1, suara2);
            }

            if (suaraSatu > suaraDua){
                wilayahMenangSatu++;
            } else if (suaraSatu < suaraDua){
                wilayahMenangDua++;
            }

            long temp = suaraSatu+suaraDua;
//            System.out.println((int)Math.floor((float)suaraSatu / temp * 100)+ " "+ suaraSatu + " "+ suaraDua);

//            System.out.println(suaraSatu+" "+suaraDua);
            if (suaraSatu+suaraDua != 0){
//                System.out.println(suaraSatu);
//                System.out.println(suaraDua);

//                System.out.println((int)Math.floor((float)suaraSatu / temp * 100));
                persentasiMenangSatu[(int)Math.floor((double) suaraSatu / (double)temp * 100)]++;
                persentasiMenangDua[(int)Math.floor((double) suaraDua / (double)temp * 100)]++;
            }
            if (suaraSatu==0 && suaraDua == 0){
                persentasiMenangSatu[50]++;
                persentasiMenangDua[50]++;
            }
//            if (suaraSatu-suara1 > suaraDua-suara2){
//                wilayahMenangSatu--;
//            } else if (suaraSatu-suara1 < suaraDua-suara2){
//                wilayahMenangDua--;
//            }

        }
        public void kurangSuara(long suara1, long suara2){
            if (suaraSatu > suaraDua){
                wilayahMenangSatu--;
            } else if (suaraSatu < suaraDua){
                wilayahMenangDua--;
            }

            if (suaraSatu+suaraDua != 0){
                long temp = suaraSatu+suaraDua;
                persentasiMenangSatu[(int)Math.floor((double) suaraSatu / (double)temp * 100)]--;
                persentasiMenangDua[(int)Math.floor((double) suaraDua / (double)temp * 100)]--;
            }
            if (suaraSatu==0 && suaraDua == 0){
                persentasiMenangSatu[50]--;
                persentasiMenangDua[50]--;
            }

            suaraSatu -= suara1;
            suaraDua -= suara2;
            if (parent != null){
                parent.kurangSuara(suara1, suara2);
            }

            if (suaraSatu > suaraDua){
                wilayahMenangSatu++;
            } else if (suaraSatu < suaraDua){
                wilayahMenangDua++;
            }
            if (suaraSatu+suaraDua != 0){
                long temp = suaraSatu+suaraDua;
                persentasiMenangSatu[(int)Math.floor((double) suaraSatu / (double)temp * 100)]++;
                persentasiMenangDua[(int)Math.floor((double) suaraDua / (double)temp * 100)]++;
            }
            if (suaraSatu==0 && suaraDua == 0){
                persentasiMenangSatu[50]++;
                persentasiMenangDua[50]++;
            }

//            if (suaraSatu-suara1 > suaraDua-suara2){
//                wilayahMenangSatu--;
//            } else if (suaraSatu-suara1 < suaraDua-suara2){
//                wilayahMenangDua--;
//            }

        }
        @Override
        public String toString() {
            return "Node{" +
                    ", suaraSatu=" + suaraSatu +
                    ", suaraDua=" + suaraDua +
                    '}';
        }
    }
    public static class AVLNode{
        long selisih;
        long banyakWilayah;
        int height;
        AVLNode left, right;

        AVLNode(long selisih){
            this.selisih = selisih;
            this.banyakWilayah = 0;
            this.height = 1;
        }
    }

//    AVL TREE ini saya gunakan dari sebuah website https://www.geeksforgeeks.org/avl-tree-set-1-insertion/

    public static class AVLTree{
        AVLNode root;

        int max(int a, int b){
            if (a > b){
                return a;
            } else {
                return b;
            }
        }
        int height(AVLNode node){
            if (node == null){
                return 0;
            } else {
                return node.height;
            }
        }
        int getPerbedaan(AVLNode node){
            if(node == null){
                return 0;
            } else {
                return height(node.left) - height(node.right);
            }
        }

        AVLNode rotateKanan(AVLNode node){
            AVLNode anakKiri = node.left;
            AVLNode kanannyaAnakKiri = anakKiri.right;

            anakKiri.right = node;
            node.left = kanannyaAnakKiri;

            node.height = max(height(node.left), height(node.right)) + 1;
            anakKiri.height = max(height(anakKiri.left), height(anakKiri.right)) + 1;

            return anakKiri;
        }

        AVLNode rotateKiri(AVLNode node){
            AVLNode anakKanan = node.right;
            AVLNode kirinyaAnakKanan = anakKanan.left;

            anakKanan.left = node;
            node.right = kirinyaAnakKanan;

            node.height = max(height(node.left), height(node.right)) + 1;
            anakKanan.height = max(height(anakKanan.left), height(anakKanan.right)) + 1;

            return anakKanan;
        }


        AVLNode insert(AVLNode node , long selisih){
            if(node == null){
                return (new AVLNode(selisih));
            }
            if (selisih < node.selisih){
                node.left = insert(node.left, selisih);
            } else if (selisih > node.selisih){
                node.right = insert(node.right, selisih);
            } else {
                return node;
            }

            node.height = 1 + max(height(node.left), height(node.right));

            int perbedaanTinggi = getPerbedaan(node);

            if (perbedaanTinggi > 1 && selisih < node.left.selisih){
                return rotateKanan(node);
            }
            if(perbedaanTinggi < -1 && selisih > node.right.selisih){
                return  rotateKiri(node);
            }
            if (perbedaanTinggi > 1 && selisih > node.left.selisih){
                node.left = rotateKiri(node.left);
                return rotateKanan(node);
            }
            if (perbedaanTinggi < -1 && selisih < node.right.selisih){
                node.right = rotateKanan(node.right);
                return rotateKiri(node);
            }
            return node;
        }
        void preOrder(AVLNode node){
            if (node!=null){
                System.out.println(node.selisih + " " + node.height);
                preOrder(node.left);
                preOrder(node.right);
//                System.out.print(node.selisih + " ");

            }

        }
        AVLNode get(long selisih){
            AVLNode current = root;

            while(current != null){
                if (selisih == current.selisih){
                    return current;
                } else if (selisih < current.selisih){
                    current = current.left;
                } else {
                    current = current.right;
                }
            }
            return null;
        }
    }

    public static void printOutput(String answer) throws IOException {
        out.println(answer);
    }

    static class InputReader {
        // taken from https://codeforces.com/submissions/Petr
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }
        public Long nextLong() {
            return Long.parseLong(next());
        }

    }
}

